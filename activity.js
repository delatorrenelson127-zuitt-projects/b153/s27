const express = require('express')

const mongoose = require('mongoose')

const app = express();

mongoose.connect("mongodb+srv://admin:admin@testclusterforrobo3t.hpoil.mongodb.net/b153?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;

db.on('error', console.error.bind(console,'connection error'));

db.once('open',()=>console.log('We are now connected to MongoDB'))

app.use(express.json());

const port = 4000;  

const dataSchema = new mongoose.Schema({
	username: String,
	password: String
})

let User = mongoose.model('users', dataSchema)


app.post('/',(req,res)=>{

	console.log(req.body)

	let newUser = new User({
		username: req.body.username,
		password: req.body.password
	})

	newUser
		.save()
		.then(result => res.send(result))
		.catch(error => res.send(error))

	console.log(newUser)

})

app.get('/users',(req,res)=>{
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
})

app.listen(port, () => console.log(`Server running at port ${port}`))