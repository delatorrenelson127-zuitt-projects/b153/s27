// importing the contents of the Express package to use for our application
const express = require('express')

// Object data modeling library for mongodb and nodejs
// that manages models/schemas and allows a quick and easy way to connect to a MongoDB database
const mongoose = require('mongoose')

const app = express();

mongoose.connect("mongodb+srv://admin:admin@testclusterforrobo3t.hpoil.mongodb.net/b153?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;

db.on('error', console.error.bind(console,'connection error'));

db.once('open',()=>console.log('We are now connected to MongoDB'))


// give the express() function from the Express package a variable 'app' 
// so that i cant be called more easily

app.use(express.json());

// declare a port number
const port = 4000;  

// confirm connection
// mongoose.connection.once('open',()=> console.log('Connected to MongoDB Atlas'))

const taskSchema = new mongoose.Schema({

	/*
		Define the fields for the task document.

		The task document should have a name and status field.

		Both fields MUST be strings.

	*/

	name: String,
	status: String

})


// create a GET route to check if Express is working
// app.get('/', (req, res)=>{
//     res.send('Hello from Express!')
// })


let Task = mongoose.model('courses', taskSchema)

// app.post('/', (req,res)=>{
//     // check the incoming request body
//     // console.log(req.body);
// })

// app.get('/about', (req, res)=>{
//     res.send('About!')
// })

app.post('/',(req,res)=>{

	//check the incoming request body
	console.log(req.body)

	let newTask = new Task({
		name: req.body.name,
		status: req.body.status
	})

	newTask
		.save()
		.then(result => res.send(result))
		.catch(error => res.send(error))

	console.log(newTask)

})

// GET Route - get all tasks document
app.get('/tasks',(req,res)=>{
	// res.send('Testing from get all tasks documents route.')
	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
})

// start the server and confirm that it is running
app.listen(port, () => console.log(`Server running at port ${port}`))